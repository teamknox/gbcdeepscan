# Deep scan for GBC
This is a colorized DEEP SCAB by Mr. N.U. who is the main coder of TeamKNOx. It is simple, but enough for fun. 

<table>
<tr>
<td><img src="./pics/dscan.jpg"></td>
</tr>
</table>


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
